# -*- coding: utf-8 -*-
import pyodbc
import time


class CrossManager():

    def __init__(self, conn_str):
        self.cnxn = pyodbc.connect(conn_str)
        self.cnxn.autocommit = True

    def executeCross(self, cross_builder):
        then = time.time()
        last = 0
        for index, count in self._step(cross_builder):
            now = time.time()
            diff = int(now - then)
            if int(diff) % 10 == 0 and int(diff) != int(last):
                print "%s %%" % int(index / count * 100)
                last = diff

    def _step(self, cross_builder):
        distinct_rows = self.cnxn.execute(
            cross_builder.build_distinct_sql()).fetchall()

        count = len(distinct_rows)
        print count
        self.cnxn.execute(cross_builder.bluid_clean_sql())

        left_uSql = cross_builder.bulild_left_update_sql()

        if cross_builder.lookup_update_field:
            lookup_uSql = cross_builder.bulild_lookup_update_sql()

        sSql = cross_builder.build_cross_sql()
        index = 0.0
        for row_distinct in distinct_rows:
            cross_list = self.cnxn.execute(sSql, row_distinct).fetchall()
            left_not_in = list()
            lookup_not_in = list()
            for cross in cross_list:
                a_id = cross.__getattribute__(cross_builder.left_id_alias)
                b_id = cross.__getattribute__(cross_builder.lookup_id_alias)
                if not (a_id in left_not_in or b_id in lookup_not_in):
                    params = list()
                    for i, f in enumerate(cross_builder.left_update_field):
                        params.append(cross.__getattribute__('left_uv_%s' % i))
                    params.append(a_id)
                    self.cnxn.execute(left_uSql, params)
                    if cross_builder.lookup_update_field:
                        params = list()
                        for i, f in enumerate(
                                cross_builder.lookup_update_field):
                            params.append(
                                cross.__getattribute__('lookup_uv_%s' % i))
                        params.append(b_id)
                        self.cnxn.execute(lookup_uSql, params)
                    left_not_in.append(a_id)
                    lookup_not_in.append(b_id)

            index += 1
            yield index, count
            self.cnxn.commit()
        self.cnxn.commit()
        self.cnxn.close()


class CrossBuilder():
    # left_table = 'ImportacionRepetidas'
    left_table_alias = 'A'
    left_id = 'A.ID'
    left_id_alias = 'A_ID'
    left_update_field = [('A.B_ID', 'B.ID'), ]
    # lookup_table = 'MasterCompleto'
    lookup_table_alias = 'B'
    lookup_id = 'B.ID'
    lookup_id_alias = 'B_ID'
    lookup_update_field = list()  # [('B.A_ID','otrocampo'), ]

    cross_fields = []

    order_by = ""
    where = list()

    def bluid_clean_sql(self):
        update_uf = ""
        for f in self.left_update_field:
            if update_uf == "":
                update_uf = "SET %s = NULL" % f[0]
            else:
                update_uf += ", %s = NULL" % f[0]

        sSql = "UPDATE %s %s %s" % (
            self.left_table, self.left_table_alias, update_uf)
        return sSql

    def build_distinct_sql(self):
        fields = ",".join(str(e[0]) for e in self.cross_fields)
        sSql = "SELECT DISTINCT %s FROM %s %s INNER JOIN %s %s ON %s %s " % (
            fields,
            self.left_table,
            self.left_table_alias,
            self.lookup_table,
            self.lookup_table_alias,
            self.build_on_sql(),
            self.build_where_sql())
        return sSql

    def build_where_sql(self):
        # build where
        sWhere = ""
        for w in self.where:
            if sWhere == "":
                sWhere = " WHERE (%s)" % w
            else:
                sWhere += " AND (%s)" % w
        return sWhere

    def build_on_sql(self):
        sOn = ""
        for c in self.cross_fields:
            if sOn == "":
                sOn += "%s = %s" % (c)
            else:
                sOn += " AND %s = %s" % (c)
        return sOn

    def build_cross_sql(self):
        sOn = self.build_on_sql()
        sWhere = self.build_where_sql()
        for c in self.cross_fields:
            if sWhere == "":
                sWhere = "WHERE %s = ?" % (c[0])
            else:
                sWhere += " AND %s = ?" % (c[0])

        # build Orderby
        if self.order_by:
            sOrderBy = "ORDER BY %s" % self.order_by
        else:
            sOrderBy = ""

        select_uf = ""
        for i, f in enumerate(self.left_update_field):
            if select_uf == "":
                select_uf = "%s as left_uv_%s" % (f[1], i)
            else:
                select_uf += ", %s as left_uv_%s" % (f[1], i)
        for i, f in enumerate(self.lookup_update_field):
            if select_uf == "":
                select_uf = "%s as lookup_uv_%s" % (f[1], i)
            else:
                select_uf += ", %s as lookup_uv_%s" % (f[1], i)

        sSql = """SELECT %s as %s , %s as %s, %s %%s
                  FROM %s %s LEFT JOIN %s %s ON %s
                  %s %s""" % (self.left_id,
                              self.left_id_alias,
                              self.lookup_id,
                              self.lookup_id_alias,
                              select_uf,
                              self.left_table,
                              self.left_table_alias,
                              self.lookup_table,
                              self.lookup_table_alias,
                              sOn, sWhere, sOrderBy)
        if len(self.lookup_update_field) > 1:
            sSql = sSql % (", %s as lookup_update_value" %
                           self.lookup_update_field[1])
        else:
            sSql = sSql % ("")

        return sSql

    def bulild_left_update_sql(self):
        update_uf = ""
        for f in self.left_update_field:
            if update_uf == "":
                update_uf = "SET %s = ?" % f[0]
            else:
                update_uf += ", %s = ?" % f[0]

        left_uSql = """UPDATE %s %s %s
                  WHERE %s = ?""" % (self.left_table,
                                     self.left_table_alias,
                                     update_uf,
                                     self.left_id,
                                     )
        return left_uSql

    def bulild_lookup_update_sql(self):
        update_uf = ""
        for f in self.lookup_update_field:
            if update_uf == "":
                update_uf = "SET %s = ?" % f[0]
            else:
                update_uf += ", %s = ?" % f[0]

        lookup_uSql = """UPDATE %s %s %s
                  WHERE %s = ?""" % (self.lookup_table,
                                     self.lookup_table_alias,
                                     update_uf,
                                     self.lookup_id,
                                     )
        return lookup_uSql
